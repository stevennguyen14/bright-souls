﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : MonoBehaviour
{

	public GameObject pickupParticle;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.CompareTag("Player"))
		{
			col.SendMessage("Heal");
			gameObject.GetComponent<Collider>().enabled = false;
			Destroy(Instantiate(pickupParticle, transform.position, transform.rotation), 1f);
			Destroy(gameObject);
		}
	}
}
