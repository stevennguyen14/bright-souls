﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM : MonoBehaviour
{
	public static GM instance;
	public Text goldUI;
	public Animator goldAnim;
	public Image buttonUI, panel, coinImage;
	public Button button;
	public Text diedText, winText ,restartText, coinText;
	int coins; 

    // Start is called before the first frame update
    void Awake()
    {
		instance = this;
		buttonUI.gameObject.SetActive(false);
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	public void GetCoin()
	{
		coins+=10;

		goldUI.text = ": " + coins;
		goldAnim.Play("coin", 0, 0);
	}

	public IEnumerator FadeInDied()
	{
		buttonUI.gameObject.SetActive(true);
		yield return new WaitForSeconds(.5f);
		for (float i = 0; i <= .8f; i += Time.deltaTime * 0.5f)
		{
			// set color with i as alpha
			buttonUI.color = new Color(0, 0, 0, i);
			yield return null;
		}
		for (float i = 0; i <= 1; i += Time.deltaTime)
		{
			// set color with i as alpha
			diedText.color = new Color(0.53f, 0, 0, i);
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);
		button.enabled = true;
		for (float i = 0; i <= 1; i += Time.deltaTime)
		{
			// set color with i as alpha
			restartText.color = new Color(255, 255, 255, i);
			yield return null;
		}
		yield return null;
	}

	public IEnumerator FadeInWin()
	{
		panel.gameObject.SetActive(false);
		buttonUI.gameObject.SetActive(true);
		yield return new WaitForSeconds(.5f);
		for (float i = 0; i <= .8f; i += Time.deltaTime * 0.5f)
		{
			// set color with i as alpha
			buttonUI.color = new Color(0, 0, 0, i);
			yield return null;
		}
		for (float i = 0; i <= 1; i += Time.deltaTime)
		{
			// set color with i as alpha
			winText.color = new Color(0.53f, 0, 0, i);
			yield return null;
		}
		coinText.text = ": " + coins;
		for (float i = 0; i <= 1; i += Time.deltaTime)
		{
			coinText.color = new Color(255, 255, 255, i);
			coinImage.color = new Color(255, 255, 255, i);
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);
		button.enabled = true;
		for (float i = 0; i <= 1; i += Time.deltaTime)
		{
			// set color with i as alpha
			restartText.color = new Color(255, 255, 255, i);
			yield return null;
		}
		yield return null;
	}

}
