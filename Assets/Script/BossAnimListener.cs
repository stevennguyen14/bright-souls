﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimListener : MonoBehaviour
{
	public Collider hitBox1, hitBox2;

	
	public void Hit1()
	{
		hitBox1.gameObject.SetActive(true);
		hitBox1.enabled = true;
	}

	public void Hit2()
	{
		hitBox2.gameObject.SetActive(true);
		hitBox2.enabled = true;
	}

	public void HideHit1()
	{
		hitBox1.gameObject.SetActive(false);
		hitBox1.enabled = false;
	}

	public void HideHit2()
	{
		hitBox2.gameObject.SetActive(false);
		hitBox2.enabled = false;
	}
}
