﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBox : MonoBehaviour
{

	public int minDamage, maxDamage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnTriggerEnter(Collider col)
	{
		Attack(col.gameObject);
	}

	void Attack(GameObject enemy)
	{
		int damage = Random.Range(minDamage, maxDamage);
		enemy.SendMessage("GetDamage", damage);
	}
}
