﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
	[Header("Stats")]
	public int maxHealth;
	public int health;
	public float attackSpeed, chasingSpeed, walkingSpeed;
	public int exp;
	public int attackDistance;
	public float waitRunInterval, attackInterval;
	public int minDropRate, maxDropRate;
	public bool isBoss;


	[Header("Caches")]
	public NavMeshAgent nav;
	public Transform player;
	public Text damageText;
	public Animator canvasAnim, anim;
	public GameObject textObject, UI;
	public Text hpUI;
	public Slider hpSlider;
	public LayerMask playerLayer;
	public GameObject hitParticle;
	public Transform[] losCubes;
	public AudioClip hurtSound;
	public Renderer[] rends;
	public GameObject winText, diedText;

	public Loot loot;
	RaycastHit hit;
	bool seePlayer = true;
	AudioSource audioSource;

	// Start is called before the first frame update
	void Start()
    {
		player = GameObject.FindWithTag("Player").transform;
		audioSource = GetComponent<AudioSource>();

		health = maxHealth;

		textObject.SetActive(false);
		hpSlider.maxValue = maxHealth;
		hpSlider.value = maxHealth;
		hpUI.text = health + "/" + maxHealth;

		StartCoroutine("WaitingAround");
	}

    // Update is called once per frame
    void FixedUpdate()
    {
		UI.transform.rotation = Camera.main.transform.rotation;

		anim.SetFloat("movement", nav.velocity.sqrMagnitude);

		for (int a = 0; a < losCubes.Length; a++)
		{
			//LINE OF SIGHT
			if (Physics.Raycast(losCubes[a].position, losCubes[a].forward, out hit, 15f, playerLayer))
			{
				Debug.DrawRay(losCubes[a].position, losCubes[a].forward * 15, Color.green);
				if (seePlayer)
				{
					//start chasing state
					StartCoroutine("Chasing");
					//stop the idle waiting around state
					StopCoroutine("WaitingAround");
					//set seeplayer bool to false, to make sure the chasing coroutine is only ran once
					seePlayer = false;
				}
				if (anim.GetBool("Dead"))
				{
					//stop chasing if dead, prevent enemy from still moving when dead on ground!
					StopCoroutine("Chasing");
				}
			}

			else
			{
				Debug.DrawRay(losCubes[a].position, losCubes[a].forward * 15, Color.red);
				
			}
		}

	}


	IEnumerator WaitingAround()
	{
		float distance = Vector3.Distance(transform.position, player.position);
		anim.SetBool("Chasing", false);
		chasingSpeed = nav.speed;
		nav.speed = walkingSpeed;
		while(distance > 0)
		{
			Vector3 randPosition = new Vector3(Random.Range(-8f, 8f), 0.5f, Random.Range(-8f, 8f));
			distance = Vector3.Distance(transform.position, player.position);
			Vector3 walkingPosition = transform.position + randPosition;
			nav.SetDestination(walkingPosition);
			yield return new WaitForSeconds(2f);
		}

	}

	IEnumerator Chasing()
	{
		bool chasing = true;
		anim.SetBool("Chasing", true);
		nav.speed = chasingSpeed;

		while (chasing)
		{
			//distance between enemy and player
			float distance = Vector3.Distance(transform.position, player.position);

			while (distance > attackDistance)
			{
				distance = Vector3.Distance(transform.position, player.position);
				nav.SetDestination(player.position);
				yield return new WaitForSeconds(waitRunInterval);
			}

			distance = Vector3.Distance(transform.position, player.position); // check player distance

			//ATTACK while distance is less than a certain distance
			while (distance < attackDistance)
			{

				distance = Vector3.Distance(transform.position, player.position);
				//print("Distance: " + distance);

				//switch between attack 1 and attack 2
				int attackType = Random.Range(1, 3);
				if (attackType == 1)
				{
					anim.SetBool("Attack" + 1, true);
					anim.SetBool("Attack" + 2, false);
				}
				else
				{
					anim.SetBool("Attack" + 1, false);
					anim.SetBool("Attack" + 2, true);
				}
				yield return new WaitForSeconds(attackInterval);
				//yield return null;

				//look at player
				Vector3 lookpos = player.position;
				lookpos.y = transform.position.y;
				//transform.LookAt(lookpos);
				transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(player.transform.position - transform.position), Time.deltaTime );
			}

			anim.SetBool("Attack1", false);
			anim.SetBool("Attack2", false);
			yield return null;
		}
	}

	void Die()
	{
		GetComponent<Collider>().enabled = false;
		UI.SetActive(false);
		StopCoroutine("Chasing");
		anim.SetBool("Dead", true);
		anim.SetBool("Chasing", false);
		anim.SetBool("Attack1", false);
		anim.SetBool("Attack2", false);

		if (isBoss)
		{
			winText.SetActive(true);
			diedText.SetActive(false);
			GM.instance.StartCoroutine("FadeInWin");
		}
	}

	void UpdateUI()
	{
		//update the health bar
		hpUI.text = health + "/" + maxHealth;
		hpSlider.value = health;

	}

	IEnumerator FlashRed()
	{
		for (int i = 0; i < rends.Length; i++)
		{
			rends[i].material.color = Color.red;
		}
		yield return new WaitForSeconds(0.15f);

		for (int i = 0; i < rends.Length; i++)
		{
			rends[i].material.color = Color.white;
		}
	}

	public void GetDamage(int damage)
	{
		//hit animation
		int whichHit = Random.Range(1, 3);
		anim.SetTrigger("hit" + whichHit);

		//set the damage ui on
		textObject.SetActive(true);

		audioSource.PlayOneShot(hurtSound);
		StartCoroutine("FlashRed");
		Destroy(Instantiate(hitParticle, transform.position + new Vector3(0,1,0), transform.rotation), 1f);

		//take damage
		health -= damage;

		UpdateUI();
		
		//rotate the text animation torwards camera
		canvasAnim.transform.rotation = Camera.main.transform.rotation;

		damageText.text = "-" + damage;
		canvasAnim.Play("Damage", 0, 0);

		if(health <= 0)
		{
			Die();
			health = 0;
			//1/5 chance to drop health potion
			loot.DropPotion(Random.Range(0,5));
			//drop coins
			loot.DropSomeLoot(Random.Range(minDropRate, maxDropRate));
		}
	}
}
