﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    [Header("Stats")]
    public float moveSpeed;
    public float strafeSpeed;
    public float blockRotationSpeed;
    public float rollSpeed;
    public int maxHealth;
    public int health;
	public int maxStamina;
	public float stamina;
	public float staminaDrainAmount;
    
    [Header("Misc")]
    //public
    public Animator anim;
	public float idleTimer;
	public Transform refCube;
	public Transform camRig;
	public GameObject target;
	public Animator char1;
	public Animator char2;
	public bool isRolling, DashStomp, hasDeductedStamina;
	public Text hpUI;
	public Slider hpSlider;
	public Text staminaUI;
	public Slider staminaSlider;
	public GameObject miniMap;
	public Image panel;
	public AudioClip restartSound, deathSound, hurtSound, hitSound, healSound;

	//private
	Rigidbody rb;
    float hori, verti, mouseHori, timeLeft, originalMoveSpeed;
	Vector3 movement;
	Vector3 refPos;
	Vector3 targetDirection;
	Vector3 targetRotation;
	bool isTargeting, isSpeedSlowed, canControl;
	AudioSource audioSource;



    // Start is called before the first frame update
    void Start()
    {
		rb = GetComponent<Rigidbody>();
		timeLeft = idleTimer;
		Cursor.visible = false;
		originalMoveSpeed = moveSpeed;
		isTargeting = false;
		isRolling = false;
		DashStomp = false;
		isSpeedSlowed = false;
		canControl = true;		
		audioSource = GetComponent<AudioSource>();

        //assign the health to max health
        health = maxHealth;
		stamina = maxStamina;

		//minimap camera is set to inactive
		miniMap.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
		//make sure player isn't already dead before assigning verti and hori
		if (canControl)
		{
			verti = Input.GetAxis("Vertical");
			hori = Input.GetAxis("Horizontal");
		}
		else
		{
			verti = 0;
			hori = 0;
		}
		mouseHori = Input.GetAxis("Mouse X");

		//set hori and verti for animation parameter
		anim.SetFloat("verti", verti);
		anim.SetFloat("hori", hori);

		movement.x = hori;
		movement.z = verti;

		//BLOCKING
		Blocking();

		//Check if blocking and reduce player speed
		CheckingIfBlocking();

		//ROLLING
		Rolling();

		//Roll check
		RollingCheck();

		//recharge stamina if used
		RechargeStamina();

		//ATTTACKING
		Attacking();

		//Movement
		rb.velocity = transform.TransformDirection(movement * moveSpeed);

		//REF Cube for player rotation
		refPos.x = hori;
		refPos.y = -1f;
		refPos.z = verti;
		refCube.localPosition = refPos;

		
        anim.SetFloat("moveSpeed", rb.velocity.sqrMagnitude);


		//Target Locking and rotation
		TargetLock();

		//Camera
		camRig.Rotate(new Vector3(0, mouseHori, 0));


		if (rb.velocity.sqrMagnitude != 0) transform.rotation = camRig.rotation; //let player rotation follow camera rotation

		if (Input.GetKeyDown(KeyCode.M))
		{
			ToggleMap();
		}

	}

	void ToggleMap()
	{
		if (!miniMap.activeInHierarchy)
		{
			miniMap.SetActive(true);
		}
		else
		{
			miniMap.SetActive(false);
		}
	}

	void TargetLock()
	{
		//re-adjust player rotation to camera forward if there is no target nearby
		if (anim.GetBool("blocking") == true && isTargeting != true)
		{
			//rotate player with camera when blocking
			//Lerp the roation from running direction to forward
			anim.transform.rotation = Quaternion.Lerp(anim.transform.rotation, Quaternion.LookRotation(Camera.main.transform.up - transform.up), Time.deltaTime * blockRotationSpeed);
		}
		else
		{
			//else just go back to looking the reference cube
			//lerp from looking forward back to running direction
			if((refCube.transform.position - anim.transform.position) != Vector3.zero)
			{
				anim.transform.rotation = Quaternion.Lerp(anim.transform.rotation, Quaternion.LookRotation(refCube.transform.position - anim.transform.position), Time.deltaTime * blockRotationSpeed);
			}
			//this prevents "Look rotation viewing vector is zero" which resets the rotation to Quaternion.Identity, causing player to look back to default rotation once standing still
			else
			{
				anim.transform.LookAt(refCube.transform);
			}
		}
	}


	void Attacking()
	{
		//attack combo
		if (Input.GetMouseButtonDown(0) && canControl){

			timeLeft = idleTimer;

			int combo = anim.GetInteger("combo");

			combo++;

			anim.SetInteger("combo", combo);
		}



		//prevent player from moving when attacking
		if (anim.GetInteger("combo") != 0 && anim.GetBool("roll") != true)
		{
			moveSpeed = 0f;
		}

	}

	void Blocking()
	{
		if (Input.GetMouseButton(1))
		{
			anim.SetBool("blocking", true);
		}
		else
		{
			anim.SetBool("blocking", false);
		}

	}

	void CheckingIfBlocking()
	{
		if(anim.GetBool("blocking") == true)
		{
			moveSpeed = strafeSpeed;
			isSpeedSlowed = true;
		}
		else
		{
			moveSpeed = originalMoveSpeed;
			isSpeedSlowed = false;
		}
	}

	void Rolling()
	{
		if (Input.GetKeyDown(KeyCode.LeftShift) && stamina > staminaDrainAmount)
		{
			anim.SetBool("roll", true);
		}
		if (Input.GetKeyDown(KeyCode.Space) && stamina > staminaDrainAmount)
		{
			anim.SetBool("dash", true);
		}
	}

	//check if finished rolling to adjust movement speed
	void RollingCheck()
	{
		if(isRolling)
		{
			moveSpeed = rollSpeed;
			//make sure the roll only deducts stamina once
			if (!hasDeductedStamina)
			{
				stamina -= staminaDrainAmount;
				UpdateStaminaUI();
				hasDeductedStamina = true;
			}

		}
		//checks to make sure speed isn't already slowed from blocking, prevents it from overlapping
		else if(!isSpeedSlowed)
		{
			moveSpeed = originalMoveSpeed;
			DashStomp = false;
		}
		if (DashStomp)
		{
			moveSpeed = 0f;
		}
	}

	void LevelUp()
	{
		char1.gameObject.SetActive(false);
		char2.gameObject.SetActive(true);

		anim = char2;
	}

	void RechargeStamina()
	{
		if(stamina < maxStamina)
		{
			stamina += 20f * Time.deltaTime;
			if(stamina > maxStamina)
			{
				stamina = 100f;
			}
			UpdateStaminaUI();
		}
	}

	void UpdateStaminaUI()
	{
		staminaUI.text = "Stamina: " + stamina + "%";
		staminaSlider.value = stamina;
	}

	IEnumerator CamShake()
	{

		float t = 0.08f;

		moveSpeed = 0f;

		while (t > 0)
		{
			t -= Time.deltaTime;
			Camera.main.transform.localPosition = Random.insideUnitSphere * 0.4f;
			yield return null;
		}

		moveSpeed = originalMoveSpeed;
	}

	void UpdateUI()
	{
		hpUI.text = "Health: " + health + "%";
		hpSlider.value = health;
	}

	void Die()
	{
		health = 0;
		//update hp ui
		UpdateUI();
		//trigger the death animation
		anim.SetBool("dead", true);
		//disable collider to player doesn't keep getting damaged and trigger hit animation
		gameObject.GetComponent<CapsuleCollider>().enabled = false;
		//diable control for player
		canControl = false;
		//enable cursor 
		Cursor.visible = true;
		panel.gameObject.SetActive(false);
	}

	public void Restart()
	{
		SceneManager.LoadScene("game");
	}

	public void Heal()
	{
		health += 40;
		if(health > maxHealth)
		{
			health = maxHealth;
		}
		audioSource.PlayOneShot(healSound);
		UpdateUI();
	}

	IEnumerator FlashRed()
	{
		var tempColor = panel.color;
		tempColor.a = 0.3f;
		panel.color = tempColor;
		yield return new WaitForSeconds(0.15f);
		tempColor.a = 0f;
		panel.color = tempColor;
	}

	public void GetDamage(int damage)
    {

		//hit animation
		if (anim.GetBool("blocking"))
		{
			anim.SetTrigger("BlockHitReact");
		}
		else
		{
			anim.SetTrigger("LightHit");
		}

		StartCoroutine("CamShake");
		audioSource.PlayOneShot(hurtSound);
		audioSource.PlayOneShot(hitSound);
		StartCoroutine("FlashRed");
		//textObject.SetActive(true);

		//take damage
		health -= damage;

		UpdateUI();

        if (health <= 0)
        {
			Die();
			audioSource.PlayOneShot(restartSound);
			audioSource.PlayOneShot(deathSound);
			GM.instance.StartCoroutine("FadeInDied");
		}
    }

}
