﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimListener : MonoBehaviour
{
	public Collider hitBox1, hitBox2, hitBox3;
	public GameObject player;
	public Player playerScript;
	public float basicRollSpeed;
	public AudioClip footR, footL, slash1, slash2, slash3;

	private AudioSource audioSource;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		playerScript = player.GetComponent<Player>();
		audioSource = GetComponentInParent<AudioSource>();
	}

   public void Hit1()
	{
		hitBox1.gameObject.SetActive(true);
		hitBox1.enabled = true;
		audioSource.PlayOneShot(slash1);
	}

	public void HideHit1()
	{
		hitBox1.gameObject.SetActive(false);
		hitBox1.enabled = false;
	}

	public void Hit2()
	{
		hitBox2.gameObject.SetActive(true);
		hitBox2.enabled = true;
		audioSource.PlayOneShot(slash2);
	}

	public void HideHit2()
	{
		hitBox2.gameObject.SetActive(false);
		hitBox2.enabled = false;
	}

	public void Hit3()
	{
		hitBox3.gameObject.SetActive(true);
		hitBox3.enabled = true;
		audioSource.PlayOneShot(slash3);
	}

	public void HideHit3()
	{
		hitBox3.gameObject.SetActive(false);
		hitBox3.enabled = false;
	}

	//PUBLIC FUNCTIONS FOR ANIMATION

	public void Roll()
	{
		playerScript.isRolling = true;
	}

	public void DashStomp()
	{
		playerScript.DashStomp = true;
	}

	public void FinishRolling()
	{
		playerScript.anim.SetBool("roll", false);
		playerScript.anim.SetBool("dash", false);
		playerScript.isRolling = false;
		playerScript.DashStomp = false;
		playerScript.hasDeductedStamina = false;
	}

	public void FootR()
	{
		audioSource.PlayOneShot(footR);
	}

	public void FootL()
	{
		audioSource.PlayOneShot(footL);
	}
}
