﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    // Start is called before the first frame update
    IEnumerator Start()
    {
		yield return new WaitForSeconds(Random.Range(0.5f,1));
		StartCoroutine("FlyToUI");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		transform.Rotate(0, 0, 1);
    }

	IEnumerator FlyToUI()
	{
		float t = 0;

		Transform refCube = Camera.main.transform.Find("CoinRef");
		transform.parent = Camera.main.transform;

		Vector3 positionA = transform.localPosition;
		Vector3 positionB = refCube.localPosition;

		while(t < 1)
		{
			transform.localPosition = Vector3.Lerp(positionA, positionB, Mathf.SmoothStep(0, 1, t));
			t += Time.deltaTime;
			yield return null;
		}
		GM.instance.GetCoin();
		Destroy(gameObject);

	}
}
