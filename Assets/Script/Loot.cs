﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot : MonoBehaviour
{

	public GameObject potion, loot;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
	 
	public void DropSomeLoot(int howMany)
	{
		for(int a = 0; a < howMany; a++)
		{
			StartCoroutine(DropLoot(loot));
		}


	}

	public void DropPotion(int num)
	{
		if(num == 1)
		{
			StartCoroutine(DropLoot(potion));
		}
	}

	IEnumerator DropLoot(GameObject item)
	{
		GameObject newLoot = Instantiate(item, transform.position, transform.rotation, this.transform);
		yield return null;

		float t = 0;
		float duration = Random.Range(1f, 3f);

		Vector3 randomPos = new Vector3(Random.Range(-3, 3), 0, Random.Range(-3, 3));
		Vector3 height = new Vector3(0, 0.5f, 0);
		while (t < 1)
		{
			newLoot.transform.localPosition = Vector3.Lerp(newLoot.transform.localPosition + height, randomPos, t);
			t += Time.deltaTime * duration;
			yield return null;
		}
	}
}
